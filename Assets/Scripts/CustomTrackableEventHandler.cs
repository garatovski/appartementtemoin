using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class CustomTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{ 
    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;
    
    public int countdownTime;
    public Text countdownDisplay;
    
    public float timerTime;
    public Text timerDisplay;
    public Animator animator;
    
    private IEnumerator CountdownToStart()
    {
        while(countdownTime > 0)
        {
            countdownDisplay.text = countdownTime.ToString();

            yield return new WaitForSeconds(1f);

            countdownTime--;
        }

        countdownDisplay.text = "C'est parti!";
        
        yield return new WaitForSeconds(1f);

        countdownDisplay.gameObject.SetActive(false);
    }
    
    private IEnumerator TimeToStart()
    {
        yield return new WaitForSeconds(5f);

        while(timerTime > 0)
        {
            timerDisplay.text = timerTime.ToString();

            yield return new WaitForSeconds(1f);

            timerTime--;
        }

        timerDisplay.text = "Temps écoulé!";
        
        yield return new WaitForSeconds(1f);

        timerDisplay.gameObject.SetActive(false);
    }
    
    protected virtual void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }
    
    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }
    
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;
        
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + 
                  " " + mTrackableBehaviour.CurrentStatus +
                  " -- " + mTrackableBehaviour.CurrentStatusInfo);

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            OnTrackingLost();
        }
    }
    
    protected virtual void OnTrackingFound()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);
            var lightComponents = mTrackableBehaviour.GetComponentsInChildren<Light>(true);
            var animatorComponents = mTrackableBehaviour.GetComponentsInChildren<Animator>(true);
            
            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas:
            foreach (var component in canvasComponents)
                component.enabled = true;
            
            // Enabled lights:
            foreach (var component in lightComponents)
                component.enabled = true;
            
            // Enabled animators:
            foreach (var component in animatorComponents)
                component.enabled = true;
            
            if (animator != null)
            {
                animator.enabled = true;
                animator.Play(stateName: "A_ContainerHUD", layer: -1, normalizedTime: 0);
                Debug.Log("Animation start playing !");
            }
            
            // Start Coroutines:
            StartCoroutine(nameof(CountdownToStart));
            StartCoroutine(nameof(TimeToStart));
        }
    }
    
    protected virtual void OnTrackingLost()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);
            var lightComponents = mTrackableBehaviour.GetComponentsInChildren<Light>(true);
            var animatorComponents = mTrackableBehaviour.GetComponentsInChildren<Animator>(true);

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas:
            foreach (var component in canvasComponents)
                component.enabled = false;
            
            // Disabled lights:
            foreach (var component in lightComponents)
                component.enabled = false;
            
            // Disabled animators:
            foreach (var component in animatorComponents)
                component.enabled = false;
            
            if (animator != null)
            {
                animator.enabled = false;
                animator.Play(stateName: "A_ContainerHUD", layer: -1, normalizedTime: 0);
                Debug.Log("Animation start playing !");
            }

            // Stop Coroutines:
            StopCoroutine(nameof(CountdownToStart));
            StopCoroutine(nameof(TimeToStart));
        }
    }
    
}
