﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionObject : MonoBehaviour
{
    private GameObject modalLight;
    private GameObject modalPC;
    private GameObject modalTV;

    private GameObject lamp;
    private GameObject pc;
    private GameObject tv;
    
        
    void Start()
    {
        modalLight = GameObject.Find("ContainerModalLight");
        modalLight.SetActive(false);
        
        modalPC = GameObject.Find("ContainerModalTV");
        modalPC.SetActive(false);
        
        modalTV = GameObject.Find("ContainerModalPC");
        modalTV.SetActive(false);
        
        lamp = GameObject.Find("Lamp");
        pc = GameObject.Find("PC");
        tv = GameObject.Find("TV");
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            if (Physics.Raycast(ray, out hit))
            {
                if (lamp)
                {
                    modalLight.SetActive(true);
                }

                if (pc) 
                {
                    modalPC.SetActive(true);
                }

                if (tv == true)
                {
                    modalTV.SetActive(true);
                }
                
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                Debug.Log("Did Hit");
            }
            else
            {
                modalLight.SetActive(false);
                modalPC.SetActive(false);
                modalTV.SetActive((false));
                
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                Debug.Log("Did not Hit");
            }
        }
    }
    
}
